package org.craft11.backup;

import java.util.logging.Level;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class CleanBackups
{
    private final nfcBackup plugin;
    
    public CleanBackups(final nfcBackup plugin) {
        this.plugin = plugin;
    }
    
    public static long getDifference(final long a, final long b, final TimeUnit units) {
        return units.convert(b - a, TimeUnit.MILLISECONDS);
    }
    
    public void clean() {
        final long now = System.currentTimeMillis();
        int bckDeleted = 0;
        for (final File f : this.recursiveListFiles(new File(this.plugin.config.bckDir))) {
            if (f.toString() != this.plugin.config.bckDir) {
                final long diffDays = getDifference(f.lastModified(), now, TimeUnit.DAYS);
                if (this.plugin.config.debug) {
                    this.plugin.sendLog(f + " : " + diffDays + " days");
                }
                if (diffDays <= this.plugin.config.daystokeep) {
                    continue;
                }
                if (f.delete()) {
                    this.plugin.sendLog(" + deleted " + f + " due to age limitation (" + diffDays + " day(s))");
                }
                else {
                    this.plugin.sendLog("Cannot delete " + f + " !");
                }
                ++bckDeleted;
            }
        }
        for (int i = 0; i < 3; ++i) {
            for (final File f2 : this.recursiveListDir(new File(this.plugin.config.bckDir))) {
                if (f2.list().length == 0) {
                    f2.delete();
                }
            }
        }
        this.plugin.sendLog(" + " + bckDeleted + " backup(s) deleted");
    }
    
    private List<File> recursiveListFiles(final File path) {
        final List<File> o = new ArrayList<File>();
        if (path.isDirectory()) {
            final File[] list = path.listFiles();
            if (list != null) {
                for (int i = 0; i < list.length; ++i) {
                    o.addAll(this.recursiveListFiles(list[i]));
                }
            }
            else {
                this.plugin.sendLog(Level.WARNING, "Cannot acces to " + path);
            }
        }
        else {
            o.add(path);
        }
        return o;
    }
    
    private List<File> recursiveListDir(final File path) {
        final List<File> o = new ArrayList<File>();
        if (path.isDirectory()) {
            o.add(path);
            final File[] list = path.listFiles();
            if (list != null) {
                for (int i = 0; i < list.length; ++i) {
                    o.addAll(this.recursiveListDir(list[i]));
                }
            }
            else {
                this.plugin.sendLog(Level.WARNING, "Cannot acces to " + path);
            }
        }
        return o;
    }
}
