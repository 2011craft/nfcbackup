package org.craft11.backup;

public class TaskBackups extends Thread
{
    private nfcBackup plugin;

    public TaskBackups(final nfcBackup plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public void run() {
        if (this.plugin.isBackupStarted || (!this.plugin.dirtied && this.plugin.getServer().getOnlinePlayers().length == 0)) {
            return;
        }
        this.plugin.executeBackup(false);
    }
}
