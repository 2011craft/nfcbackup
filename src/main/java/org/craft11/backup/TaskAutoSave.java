package org.craft11.backup;

import org.bukkit.World;

public class TaskAutoSave extends Thread
{
    private final nfcBackup plugin;
    private final boolean save;
    
    public TaskAutoSave(final nfcBackup plugin, final boolean save) {
        this.plugin = plugin;
        this.save = save;
    }
    
    @Override
    public void run() {
        if (!this.save) {
            nfcBackup.Instance.getServer().savePlayers();
        }
        for (final World world : nfcBackup.Instance.getServer().getWorlds()) {
            if (!this.save) {
                world.save();
            }
        }
        this.plugin.spawnBackupStage(this.save ? "done" : "backup", false);
    }
}
