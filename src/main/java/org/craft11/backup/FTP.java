package org.craft11.backup;

import java.util.StringTokenizer;
import java.util.ArrayList;
import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.net.Socket;
import java.util.logging.Logger;

public class FTP
{
    private Logger log;
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    
    public FTP(final Logger log) {
        this.socket = null;
        this.reader = null;
        this.writer = null;
        (this.log = log).info("FTP initialized");
    }
    
    public synchronized void connect(final String host) throws IOException {
        this.log.info("Connect withour any informations");
        this.connect(host, 21);
    }
    
    public synchronized void connect(final String host, final int port) throws IOException {
        this.log.info("Connect annonymous");
        this.connect(host, port, "anonymous", "anonymous");
    }
    
    public synchronized void connect(final String host, final int port, final String user, final String pass) throws IOException {
        this.log.info("Connect with user and pass");
        if (this.socket != null) {
            this.log.warning("already connected");
            throw new IOException("FTP is already connected. Disconnect first.");
        }
        this.socket = new Socket(host, port);
        this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
        String response = this.readLine();
        if (!response.startsWith("220 ")) {
            this.log.warning(response);
            throw new IOException("FTP received an unknown response when connecting to the FTP server: " + response);
        }
        this.sendLine("USER " + user);
        response = this.readLine();
        if (!response.startsWith("331 ")) {
            this.log.warning(response);
            throw new IOException("FTP received an unknown response after sending the user: " + response);
        }
        this.sendLine("PASS " + pass);
        response = this.readLine();
        if (!response.startsWith("230 ")) {
            this.log.warning(response);
            throw new IOException("FTP was unable to log in with the supplied password: " + response);
        }
    }
    
    public synchronized void disconnect() throws IOException {
        try {
            this.sendLine("QUIT");
        }
        finally {
            this.socket = null;
        }
        this.socket = null;
    }
    
    public synchronized String pwd() throws IOException {
        this.sendLine("PWD");
        String dir = null;
        final String response = this.readLine();
        if (response.startsWith("257 ")) {
            final int firstQuote = response.indexOf(34);
            final int secondQuote = response.indexOf(34, firstQuote + 1);
            if (secondQuote > 0) {
                dir = response.substring(firstQuote + 1, secondQuote);
            }
        }
        return dir;
    }
    
    public synchronized boolean cwd(final String dir) throws IOException {
        this.sendLine("CWD " + dir);
        final String response = this.readLine();
        return response.startsWith("250 ");
    }
    
    public synchronized boolean stor(final File file) throws IOException {
        if (file.isDirectory()) {
            throw new IOException("FTP cannot upload a directory.");
        }
        final String filename = file.getName();
        return this.stor(new FileInputStream(file), filename);
    }
    
    public synchronized boolean stor(final InputStream inputStream, final String filename) throws IOException {
        final BufferedInputStream input = new BufferedInputStream(inputStream);
        final Socket dataSocket = this.passive();
        this.sendLine("STOR " + filename);
        String response = this.readLine();
        if (!response.startsWith("150 ")) {
            throw new IOException("FTP was not allowed to send the file: " + response);
        }
        final BufferedOutputStream output = new BufferedOutputStream(dataSocket.getOutputStream());
        final byte[] buffer = new byte[4096];
        int bytesRead = 0;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        output.flush();
        output.close();
        input.close();
        response = this.readLine();
        return response.startsWith("226 ");
    }
    
    public synchronized boolean bin() throws IOException {
        this.sendLine("TYPE I");
        final String response = this.readLine();
        return response.startsWith("200 ");
    }
    
    public synchronized boolean ascii() throws IOException {
        this.sendLine("TYPE A");
        final String response = this.readLine();
        return response.startsWith("200 ");
    }
    
    public synchronized String[] help() throws IOException {
        this.sendLine("HELP");
        for (int nlCount = 0; nlCount < 2; ++nlCount) {
            final String response = this.readLine();
            if (response.charAt(response.length() - 1) == '.') {}
        }
        return null;
    }
    
    public synchronized String[] ls() throws IOException {
        final Socket sok = this.passive();
        this.sendLine("LIST");
        String response = this.readLine();
        if (!response.startsWith("150 ")) {
            throw new IOException("Could not read the directory");
        }
        response = this.readLine();
        if (!response.startsWith("226 ")) {
            throw new IOException("Could not read the directory");
        }
        final ArrayList<String> files = new ArrayList<String>();
        final String[] ls = this.readInputStream(sok.getInputStream());
        for (int i = 0; i < ls.length; ++i) {
            files.add(ls[i].substring(ls[i].lastIndexOf(32)));
        }
        return files.toArray(new String[files.size()]);
    }
    
    public synchronized boolean delete(final String filename) throws IOException {
        this.sendLine("DELE " + filename);
        final String response = this.readLine();
        if (!response.startsWith("250 ")) {
            throw new IOException("FTP is not allowed to delete this file");
        }
        return true;
    }
    
    private String[] readInputStream(final InputStream is) throws IOException {
        final ArrayList<String> a = new ArrayList<String>();
        final BufferedReader r = new BufferedReader(new InputStreamReader(is));
        String s;
        while ((s = r.readLine()) != null) {
            a.add(s);
        }
        return a.toArray(new String[a.size()]);
    }
    
    private synchronized Socket passive() throws IOException {
        this.sendLine("PASV");
        final String response = this.readLine();
        if (!response.startsWith("227 ")) {
            throw new IOException("FTP could not request passive mode: " + response);
        }
        String ip = null;
        int port = -1;
        final int opening = response.indexOf(40);
        final int closing = response.indexOf(41, opening + 1);
        if (closing > 0) {
            final String dataLink = response.substring(opening + 1, closing);
            final StringTokenizer tokenizer = new StringTokenizer(dataLink, ",");
            try {
                ip = String.valueOf(tokenizer.nextToken()) + "." + tokenizer.nextToken() + "." + tokenizer.nextToken() + "." + tokenizer.nextToken();
                port = Integer.parseInt(tokenizer.nextToken()) * 256 + Integer.parseInt(tokenizer.nextToken());
            }
            catch (Exception e) {
                throw new IOException("FTP received bad data link information: " + response);
            }
        }
        return new Socket(ip, port);
    }
    
    private void sendLine(final String line) throws IOException {
        if (this.socket == null) {
            throw new IOException("FTP is not connected.");
        }
        try {
            this.writer.write(String.valueOf(line) + "\r\n");
            this.writer.flush();
        }
        catch (IOException e) {
            this.socket = null;
            throw e;
        }
    }
    
    private String readLine() throws IOException {
        final String line = this.reader.readLine();
        return line;
    }
}
