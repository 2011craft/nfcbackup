package org.craft11.backup;

import org.bukkit.World;
import java.util.Arrays;
import java.util.Map;
import org.bukkit.ChatColor;
import java.util.ArrayList;
import java.io.File;
import java.util.List;
import org.bukkit.util.config.Configuration;

public class Config
{
    private final nfcBackup plugin;
    private Configuration cfg;
    public List<String> worlds;
    public String bckDir;
    public String bckTempDir;
    public String bckFormat;
    public long interval;
    public long firstDelay;
    public long daystokeep;
    public boolean backupPlugins;
    public boolean debug;
    public boolean msg_enable;
    public String msg_BackupStarted;
    public String msg_BackupEnded;
    public String msg_ManualBackupStarted;
    public boolean compressionEnabled;
    public int compressionMode;
    public int compressionLevel;
    
    public Config(final nfcBackup plugin) {
        this.plugin = plugin;
        this.loadConfig();
    }
    
    public void loadConfig() {
        try {
            this.plugin.sendLog("Loading configuration...");
            boolean rewrite = false;
            final String[] allowedKeys = { "worlds", "backup.dir", "backup.temp-dir", "backup.format", "time.interval", "time.delay", "time.days-to-keep", "options.backup-plugins", "options.debug", "messages.backup-started", "messages.backup-started-manual", "messages.backup-ended", "messages.enabled", "compression.enabled", "compression.level", "compression.mode" };
            (this.cfg = new Configuration(new File(this.plugin.getDataFolder() + "/config.yml"))).load();
            this.worlds = (List<String>)this.cfg.getStringList("worlds", (List)new ArrayList());
            this.bckDir = this.cfg.getString("backup.dir", "backups");
            this.bckTempDir = this.cfg.getString("backup.temp-dir", "backups_temp");
            this.bckFormat = this.cfg.getString("backup.format", "%W/%Y-%M-%D_%H-%m-%S");
            this.interval = this.cfg.getInt("time.interval", -1);
            this.firstDelay = this.cfg.getInt("time.delay", -1);
            this.daystokeep = this.cfg.getInt("time.days-to-keep", -1);
            this.debug = this.cfg.getBoolean("options.debug", false);
            this.backupPlugins = this.cfg.getBoolean("options.backup-plugins", true);
            this.msg_enable = this.cfg.getBoolean("messages.enabled", true);
            this.msg_BackupEnded = this.cfg.getString("messages.backup-ended", ChatColor.AQUA + "[nfcBackup] Backup finished!");
            this.msg_BackupStarted = this.cfg.getString("messages.backup-started", ChatColor.AQUA + "[nfcBackup] Running backup...");
            this.msg_ManualBackupStarted = this.cfg.getString("messages.backup-started-manual", ChatColor.AQUA + "[nfcBackup] Running manual backup...");
            this.compressionEnabled = this.cfg.getBoolean("compression.enabled", true);
            String s_compressionMode = this.cfg.getString("compression.mode", (String)null);
            String s_compressionLevel = this.cfg.getString("compression.level", (String)null);
            this.compressionLevel = 9;
            this.compressionMode = 8;
            int i = 0;
            for (final Map.Entry<String, Object> entry : this.cfg.getAll().entrySet()) {
                final String key = entry.getKey();
                if (!Arrays.asList(allowedKeys).contains(key)) {
                    this.cfg.removeProperty(key);
                    ++i;
                }
            }
            if (i > 0) {
                rewrite = true;
                this.plugin.sendLog("Removed " + i + " unknown key(s)");
            }
            if (this.compressionEnabled) {
                if (s_compressionLevel == null) {
                    s_compressionLevel = "BEST_COMPRESSION";
                    this.cfg.setProperty("compression.level", (Object)s_compressionLevel);
                    rewrite = true;
                }
                else if (s_compressionLevel == "BEST_COMPRESSION") {
                    this.compressionLevel = 9;
                }
                else if (s_compressionLevel == "BEST_SPEED") {
                    this.compressionLevel = 1;
                }
                else if (s_compressionLevel == "NO_COMPRESSION") {
                    this.compressionLevel = 0;
                }
                if (s_compressionMode == null) {
                    s_compressionMode = "DEFLATED";
                    this.cfg.setProperty("compression.mode", (Object)s_compressionMode);
                    rewrite = true;
                }
            }
            if (this.worlds.isEmpty()) {
                for (final World w : this.plugin.getServer().getWorlds()) {
                    this.worlds.add(w.getName());
                }
                this.cfg.setProperty("worlds", (Object)this.worlds);
                rewrite = true;
            }
            if (this.interval <= 0L) {
                this.interval = 3600L;
                this.cfg.setProperty("time.interval", (Object)this.interval);
                rewrite = true;
            }
            if (this.firstDelay < 0L) {
                this.firstDelay = 10L;
                this.cfg.setProperty("time.delay", (Object)this.firstDelay);
                rewrite = true;
            }
            if (this.daystokeep < 0L) {
                this.daystokeep = 5L;
                this.cfg.setProperty("time.days-to-keep", (Object)this.firstDelay);
                rewrite = true;
            }
            this.interval *= 20L;
            this.firstDelay *= 20L;
            if (rewrite) {
                String headerText = "# available worlds :\r\n";
                for (final World w2 : this.plugin.getServer().getWorlds()) {
                    headerText = String.valueOf(headerText) + "# - " + w2.getName() + "\r\n";
                }
                this.cfg.setHeader(headerText);
                this.cfg.save();
            }
            this.plugin.sendLog(String.valueOf(this.worlds.size()) + " worlds loaded.");
        }
        catch (Exception e) {
            this.plugin.logException(e, "Error while loading config");
        }
    }
}
