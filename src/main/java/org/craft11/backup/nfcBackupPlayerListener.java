package org.craft11.backup;

import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerListener;

public class nfcBackupPlayerListener extends PlayerListener
{
    private final nfcBackup plugin;
    
    public nfcBackupPlayerListener(final nfcBackup plugin) {
        this.plugin = plugin;
    }
    
    public void onPlayerQuit(final PlayerQuitEvent event) {
        this.plugin.dirtied = true;
    }
}
