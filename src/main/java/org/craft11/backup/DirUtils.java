package org.craft11.backup;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;

public class DirUtils
{
    public static void deleteDirectory(final File tarDir) {
        if (tarDir.isDirectory()) {
            File[] listFiles;
            for (int length = (listFiles = tarDir.listFiles()).length, i = 0; i < length; ++i) {
                final File child = listFiles[i];
                deleteDirectory(child);
            }
        }
        tarDir.delete();
    }
    
    public static void copyDirectory(final File srcDir, final File destDir) throws FileNotFoundException, IOException {
        destDir.mkdirs();
        String[] list;
        for (int length = (list = srcDir.list()).length, i = 0; i < length; ++i) {
            final String child = list[i];
            final File srcFile = new File(srcDir, child);
            if (srcFile.isDirectory()) {
                copyDirectory(srcFile, new File(destDir, child));
            }
            else {
                copyFile(srcFile, new File(destDir, child));
            }
        }
    }
    
    public static void copyFile(final File srcFile, final File destFile) throws FileNotFoundException, IOException {
        final InputStream inStream = new FileInputStream(srcFile);
        final OutputStream outStream = new FileOutputStream(destFile);
        try {
            final byte[] buf = new byte[1024];
            int len;
            while ((len = inStream.read(buf)) > -1) {
                if (len > 0) {
                    outStream.write(buf, 0, len);
                }
            }
        }
        finally {
            inStream.close();
            outStream.close();
        }
        inStream.close();
        outStream.close();
    }
}
