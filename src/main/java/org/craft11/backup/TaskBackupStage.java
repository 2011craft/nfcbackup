package org.craft11.backup;

import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;
import java.util.logging.Level;
import java.io.File;
import org.bukkit.World;

public class TaskBackupStage extends Thread
{
    private final nfcBackup plugin;
    private final String stage;
    private final boolean manual;
    
    public TaskBackupStage(final nfcBackup plugin, final String stage, final boolean manual) {
        this.plugin = plugin;
        this.stage = stage;
        this.manual = manual;
    }
    
    @Override
    public void run() {
        if ("begin".equals(this.stage)) {
            this.begin();
        }
        else if ("backup".equals(this.stage)) {
            this.backup();
        }
        else {
            this.done();
        }
    }
    
    private void begin() {
        if (manual) {
            this.SendMessage(this.plugin.config.msg_ManualBackupStarted);
        }
        else {
            this.SendMessage(this.plugin.config.msg_BackupStarted);
        }
        this.plugin.spawnAutoSave(false, 60L);
    }
    
    private void backup() {
        for (final World world : nfcBackup.Instance.getServer().getWorlds()) {
            if (plugin.config.worlds.contains(world.getName()))
                this.doBackup(world, world.getName());
        }
        this.deleteDir(new File(this.plugin.config.bckTempDir));
        this.plugin.spawnAutoSave(true, 0L);
    }
    
    private void done() {
        if (this.plugin.config.daystokeep > 0L) {
            if (this.plugin.shouldCleanBackups) {
                new CleanBackups(this.plugin).clean();
            }
            this.plugin.shouldCleanBackups = !this.plugin.shouldCleanBackups;
        }
        this.SendMessage(this.plugin.config.msg_BackupEnded);
        this.plugin.dirtied = false;
        this.plugin.isBackupStarted = false;
    }
    
    private void SendMessage(final String msg) {
        if (this.plugin.config.msg_enable) {
            this.plugin.getServer().broadcastMessage(msg);
        }
        else {
            this.plugin.sendLog(msg);
        }
    }
    
    private void doBackup(final World world, final String name) {
        final String format = this.getFormat(world);
        final File worldDir = new File(".", name);
        if (this.plugin.config.compressionEnabled) {
            this.plugin.sendLog(" * " + name + " -- compressing");
            final File tempDir = new File(this.plugin.config.bckTempDir, format);
            if (this.copyDir(worldDir, new File(tempDir, name))) {
                this.compressDir(tempDir, new File(this.plugin.config.bckDir, format + ".zip"));
            }
        }
        else {
            this.plugin.sendLog(" * " + name + " -- copying");
            this.copyDir(worldDir, new File(this.plugin.config.bckDir, format));
        }
    }
    
    private boolean copyDir(final File srcDir, final File destDir) {
        try {
            DirUtils.copyDirectory(srcDir, destDir);
        }
        catch (Exception ex) {
            this.plugin.sendLog("\t\\ failed");
            this.plugin.logException(ex);
            this.deleteDir(destDir);
            return false;
        }
        return true;
    }
    
    private void deleteDir(final File tarDir) {
        DirUtils.deleteDirectory(tarDir);
        if (tarDir.exists()) {
            this.plugin.sendLog(Level.WARNING, "unable to delete directory: " + tarDir);
        }
    }
    
    private void compressDir(final File tempDir, final File destFile) {
        try {
            ZipUtils.zipDir(tempDir, destFile, this.plugin.config.compressionMode, this.plugin.config.compressionLevel);
        }
        catch (Exception ex) {
            this.plugin.sendLog("\t\\ failed");
            this.plugin.logException(ex);
            destFile.delete();
        }
    }
    
    private String padZero(final int i) {
        return String.format("%02d", i).toString();
    }
    
    public String getFormat(final World world) {
        final Calendar date = Calendar.getInstance();
        final Map<String, String> formats = new HashMap<String, String>();
        formats.put("%Y", this.padZero(date.get(1)));
        formats.put("%M", this.padZero(date.get(2) + 1));
        formats.put("%D", this.padZero(date.get(5)));
        formats.put("%H", this.padZero(date.get(11)));
        formats.put("%m", this.padZero(date.get(12)));
        formats.put("%S", this.padZero(date.get(13)));
        formats.put("%W", world.getName());
        formats.put("%U", world.getUID().toString());
        formats.put("%s", String.valueOf(world.getSeed()));
        String format = this.plugin.config.bckFormat;
        for (final Map.Entry<String, String> entry : formats.entrySet()) {
            format = format.replaceAll(entry.getKey(), entry.getValue());
        }
        return format;
    }
}
