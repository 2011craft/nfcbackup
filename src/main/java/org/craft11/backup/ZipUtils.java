package org.craft11.backup;

import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.zip.ZipOutputStream;
import java.io.BufferedOutputStream;
import java.util.zip.Checksum;
import java.io.OutputStream;
import java.util.zip.CheckedOutputStream;
import java.util.zip.Adler32;
import java.io.FileOutputStream;
import java.io.File;

public class ZipUtils
{
    private static final int BUFFER_SIZE = 8192;
    
    public static void zipDir(final File srcDir, final File destFile, final int method, final int level) throws FileNotFoundException, IOException {
        destFile.getParentFile().mkdirs();
        final FileOutputStream outStream = new FileOutputStream(destFile);
        try {
            final CheckedOutputStream checkedOutStream = new CheckedOutputStream(outStream, new Adler32());
            try {
                final BufferedOutputStream bufOutStream = new BufferedOutputStream(checkedOutStream, 8192);
                try {
                    final ZipOutputStream zipOutStream = new ZipOutputStream(bufOutStream);
                    try {
                        try {
                            zipOutStream.setMethod(method);
                            zipOutStream.setLevel(level);
                        }
                        catch (Exception ex) {}
                        zipDir(srcDir, "", zipOutStream);
                    }
                    finally {
                        zipOutStream.close();
                    }
                    zipOutStream.close();
                }
                finally {
                    bufOutStream.close();
                }
                bufOutStream.close();
            }
            finally {
                checkedOutStream.close();
            }
            checkedOutStream.close();
        }
        finally {
            outStream.close();
        }
        outStream.close();
    }
    
    private static void zipDir(final File srcDir, String currentDir, final ZipOutputStream zipOutStream) throws FileNotFoundException, IOException {
        if (!"".equals(currentDir)) {
            currentDir = String.valueOf(currentDir) + "/";
            zipOutStream.putNextEntry(new ZipEntry(currentDir));
            zipOutStream.closeEntry();
        }
        final File zipDir = new File(srcDir, currentDir);
        String[] list;
        for (int length = (list = zipDir.list()).length, i = 0; i < length; ++i) {
            final String child = list[i];
            final File srcFile = new File(zipDir, child);
            if (srcFile.isDirectory()) {
                zipDir(srcDir, String.valueOf(currentDir) + child, zipOutStream);
            }
            else {
                final ZipEntry zipEntry = new ZipEntry(String.valueOf(currentDir) + child);
                zipEntry.setTime(srcFile.lastModified());
                zipFile(srcFile, zipEntry, zipOutStream);
            }
        }
    }
    
    private static void zipFile(final File srcFile, final ZipEntry zipEntry, final ZipOutputStream zipOutStream) throws FileNotFoundException, IOException {
        final InputStream inStream = new FileInputStream(srcFile);
        try {
            final BufferedInputStream bufInStream = new BufferedInputStream(inStream, 8192);
            try {
                zipOutStream.putNextEntry(zipEntry);
                final byte[] buf = new byte[8192];
                int len;
                while ((len = bufInStream.read(buf)) > -1) {
                    if (len > 0) {
                        zipOutStream.write(buf, 0, len);
                    }
                }
                zipOutStream.closeEntry();
            }
            finally {
                bufInStream.close();
            }
            bufInStream.close();
        }
        finally {
            inStream.close();
        }
        inStream.close();
    }
}
