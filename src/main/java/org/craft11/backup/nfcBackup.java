package org.craft11.backup;

import org.bukkit.ChatColor;
import java.util.logging.Level;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Event;
import org.bukkit.plugin.Plugin;
import java.util.logging.Logger;
import org.bukkit.plugin.java.JavaPlugin;

public class nfcBackup extends JavaPlugin
{
    public Config config;
    private final Logger log;
    public boolean isBackupStarted;
    public boolean dirtied;
    public boolean shouldCleanBackups;
    public static nfcBackup Instance;
    
    public nfcBackup() {
        this.log = Logger.getLogger("Minecraft");
    }
    
    public void onDisable() {
        this.getServer().getScheduler().cancelTasks((Plugin)this);
        this.sendLog("v" + this.getDescription().getVersion() + " disabled");
    }
    
    public void onEnable() {
        Instance = this;
        this.config = new Config(this);
        this.isBackupStarted = false;
        this.dirtied = true;
        this.shouldCleanBackups = true;
        this.getServer().getScheduler().scheduleAsyncRepeatingTask(this, new TaskBackups(this), this.config.firstDelay, this.config.interval);
        this.getServer().getPluginManager().registerEvent(Event.Type.PLAYER_QUIT, new nfcBackupPlayerListener(this), Event.Priority.Monitor, this);
        this.sendLog("v" + this.getDescription().getVersion() + " enabled.");
    }
    
    public void executeBackup(boolean manual) {
        if (this.isBackupStarted) {
            return;
        }
        this.isBackupStarted = true;
        this.spawnBackupStage("begin", manual);
    }
    
    public void spawnAutoSave(final boolean save, final long delay) {
        this.getServer().getScheduler().scheduleSyncDelayedTask(this, new TaskAutoSave(this, save), delay);
    }
    
    public void spawnBackupStage(final String stage, boolean manual) {
        this.getServer().getScheduler().scheduleAsyncDelayedTask(this, new TaskBackupStage(this, stage, manual));
    }
    
    public void sendLog(final String msg) {
        this.sendLog(Level.INFO, msg);
    }
    
    public void sendLog(final Level level, final String msg) {
        this.log.log(level, "[" + this.getDescription().getName() + "] " + ChatColor.stripColor(msg));
    }
    
    public void logException(final Throwable ex, final String msg) {
        this.sendLog(Level.SEVERE, "---------------------------------------");
        if (!"".equals(msg)) {
            this.sendLog(Level.SEVERE, "debug: " + msg);
        }
        this.sendLog(Level.SEVERE, ex.toString() + " : " + ex.getLocalizedMessage());
        StackTraceElement[] stackTrace;
        for (int length = (stackTrace = ex.getStackTrace()).length, i = 0; i < length; ++i) {
            final StackTraceElement stack = stackTrace[i];
            this.sendLog(Level.SEVERE, "\t" + stack.toString());
        }
        this.sendLog(Level.SEVERE, "---------------------------------------");
    }
    
    public void logException(final Throwable e) {
        this.logException(e, "");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equals("backup")) {
            executeBackup(true);
            return true;
        }
        return false;
    }
}
